# Demo project - checking card validity 

## Development

### `npm run start:dev`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the `jest` test runner.<br>

## Production

Build the app using the command:

### `npm run build`
Builds the app for production to the `dist` folder.<br><br>

Run the app in production with:

### `npm run start:prod`

Needs to be run after `build` <br>
Starts the app with: `node dist/server.js`