import App from "./src/core/app";
import Database from "./src/core/database";

const app = new App();
app.run();


process.on('uncaughtException', async function(err) {
    // graceful shutdown
    await (Database.getInstance()).stop();
    await app.stop();
})


