import {Response, Request, Router, NextFunction} from "express";
import {param, validationResult} from "express-validator";
import {apiKeyAuthMiddleware} from "../core/auth";
import {CardService} from "./card.service";
import {BadRequestError} from "../core/errors";

export class CardController {
	private router: Router = Router();

	private cardService: CardService;

	private prefix = '/card';

	public constructor() {
		this.initRoutes();
		this.cardService = new CardService();
	}

	public getRouter(): Router {
		return this.router;
	}

	private initRoutes() {
		this.router.get(`${this.prefix}/:cardId`,
			apiKeyAuthMiddleware,
			[
				param('cardId').isNumeric()
			],
			(req: Request, res: Response, next: NextFunction) => {
			const validationErrors = validationResult(req);
			if (!validationErrors.isEmpty()) {
				const error = validationErrors.array({onlyFirstError: true})[0];
				return next(new BadRequestError(`${error.msg} - parameter: ${error.param}`));
			}
		  return this.cardService.handleCheckCard(req, res);
		});
	}

	
}