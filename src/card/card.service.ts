import { Response, Request } from "express";
import * as fetch from 'isomorphic-fetch';

interface CardCheckStateResponse {
    state_id: string;
    state_description: string;
}

interface CardCheckValidityResponse {
    validity_start: string;
    validity_end: string;
}

export class CardService {

    public async handleCheckStatus(cardId: string): Promise<CardCheckStateResponse> {
        const response = await fetch(`http://private-264465-litackaapi.apiary-mock.com/cards/${cardId}/state`);
        return response.json();
    }

    public async handleCheckValidity(cardId: string): Promise<CardCheckValidityResponse> {
        const response = await fetch(`http://private-264465-litackaapi.apiary-mock.com/cards/${cardId}/validity`);
        return response.json();
    }

    public async handleCheckCard(req: Request, res: Response): Promise<void> {

        const response = {
            state: '',
            validity_end: ''
        };

        const validity = await this.handleCheckValidity(req.params.cardId);
        const state = await this.handleCheckStatus(req.params.cardId);

        if (state) {
            response.state = state.state_description;
        }

        if (validity) {
            response.validity_end = new Intl.DateTimeFormat('cs')
                .format(Date.parse(validity.validity_end))
                .replace(/\s/g, "");
        }

        res.json(response);
    }
}