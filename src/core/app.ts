import * as express from "express";
import * as http from "http";
import * as pino from 'pino';
import * as cors from "cors";
import {BaseError, NotFoundError} from "./errors";
import {CardController} from "../card/card.controller";
import {seed} from "../fixture/user.fixture";
import Database from "./database";

export default class App {

    public express: express.Application = express();

    private port: number = parseInt(process.env.PORT, 10) || 3000;

    private server: http.Server;

    private databaseInstance: Database;

    private logger = pino({

    });

    public async run(): Promise<void> {
        try {
            this.logger.info(`Server starting at PORT: ${this.port}`);

            await this.database();
            this.middlewares();
            this.routes();

            this.server = this.setupHttp();

            this.server.on("error", (err: Error) => {
                this.logger.error("Could not start a server", err);
            });

            this.server.listen(this.port, () => {
                this.logger.info(`Listening at http://localhost:${this.port}/`);
            });
        } catch (error) {
            this.logger.error(error);
        }
    }

    public setupHttp(): http.Server {
        return http.createServer(this.express);
    }

    public stop = async (): Promise<void> => {
        this.server.close();
    }

    private database = async () => {
        this.databaseInstance = Database.getInstance();
        await (this.databaseInstance).connect();
        // NOTE: just for testing example -> together with Mongo in memory
        await seed();
    }

    public getDatabase(): Database {
        return this.databaseInstance;
    }

    private middlewares = (): void => {
        this.express.use(cors());
    }

    private routes = (): void => {

        const cardController = new CardController();
        this.express.use(cardController.getRouter());

        this.express.get('/health-check', (req: express.Request, res: express.Response, next: express.NextFunction) => {
            this.logger.debug('Health check');
            res.json({
                status: "ok",
            });
        });

        this.express.use("*", (req: express.Request, res: express.Response) => {
            this.logger.warn(`Non existing route: ( ${req.method} ) ${req.originalUrl}`);
            throw new NotFoundError("Route not found.");
        });

        // use centralized error handler
        this.express.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {

            if (!(err instanceof BaseError)) {
                this.logger.error(err);
                res.status(500)
                    .json({message: 'Unknown error'});
            }

            this.logger.error(`Status: ${err.status}: ${err.message}`);

            res.status(err.status)
                .send({message: err.message});
        });
    }

}
