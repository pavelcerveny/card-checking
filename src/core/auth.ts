import { NextFunction, Response, Request } from "express";
import { UnauthorizedError } from "./errors";
import { User } from "../models/user/user.model";

export const apiKeyAuthMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  
	const apiToken = req.header('x-api-key') || req.header('x-token');

	if (apiToken) {
		const user = await User.findOne({ apiToken }).exec();

		if (user) {
			return next();
		}
	}

  	return next(new UnauthorizedError('Unauthorized'));
}