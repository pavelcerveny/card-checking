import * as mongoose from "mongoose";
import {MongoMemoryServer} from 'mongodb-memory-server';

export default class Database {
    private static instance: Database|null = null;

    private mongod;

    private connected = false;

    private constructor() {
        this.mongod = new MongoMemoryServer();
    }

    public static getInstance(): Database {
        if (Database.instance) {
            return Database.instance;
        } else {
            Database.instance = new Database();
            return Database.instance;
        }
    }

    public async connect() {
        if (!this.connected) {
            const uri = await this.mongod.getConnectionString();

            const mongooseOpts = {
                useNewUrlParser: true,
                useUnifiedTopology: true
            };

            await mongoose.connect(uri, mongooseOpts);
            this.connected = true;
        }
    }

    public async stop() {
        await mongoose.connection.dropDatabase();
        await mongoose.connection.close();
        await this.mongod.stop();
    }

    public async clearDatabase() {
        await mongoose.connection.dropDatabase();
        // const collections = mongoose.connection.collections;
        //
        // for (const key in collections) {
        //     const collection = collections[key];
        //     await collection.deleteMany();
        // }
    }
}