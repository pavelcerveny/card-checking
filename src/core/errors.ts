export class BaseError extends Error {
  constructor(private readonly status, public readonly message) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.status = status;
  }
}

export class UnauthorizedError extends BaseError {
  constructor(message) {
    super(401, message);
  }
}

export class NotFoundError extends BaseError {
  constructor(message) {
    super(404, message);
  }
}

export class BadRequestError extends BaseError {
  constructor(message) {
    super(400, message);
  }
}