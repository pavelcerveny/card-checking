import { User } from "../models/user/user.model";
import * as pino from 'pino';
const logger = pino();

const user1 = new User({ apiToken: 'random---test' });
const user2 = new User({ apiToken: 'random---test2' });

export async function seed() {
	try {
		await user1.save();
		await user2.save();

		logger.debug('Seed completed');
	} catch (e) {
		// ignore
		console.log(e);
	}
}