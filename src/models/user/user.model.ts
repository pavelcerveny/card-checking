import * as mongoose from 'mongoose';

const schema = new mongoose.Schema({
 apiToken: { type: String, required: true },
 createdAt: { type: Date, default: Date.now }
});

export const User = mongoose.model('User', schema);