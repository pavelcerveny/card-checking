import Database from "../src/core/database";
import {seed} from "../src/fixture/user.fixture";
import App from "../src/core/app";
import * as request2 from "supertest";


describe('App server Test', () => {

    let database: Database = null;
    let app: App = null;
    let request = null;

    beforeAll(async () => {
        app = new App();
        await app.run();

        // this is test db anyway -> will be always dropped
        // mocked mongoose should be used in the real project
        database = app.getDatabase();
        await seed();
        request = request2(app.express);
    });

    it('should return the app health', async (done) => {
        const res = await request.get('/health-check');
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('status')
        expect(res.body.status).toBe('ok');
        done();
    });

    it('should return unknown route', async (done) => {
        const res = await request.get('/test');
        expect(res.status).toBe(404);
        expect(res.body.message).toBe('Route not found.');
        done();
    });

    afterAll(() => {
        database.stop();
        app.stop();
    })
})