import Database from "../src/core/database";
import {seed} from "../src/fixture/user.fixture";
import App from "../src/core/app";
import * as request2 from "supertest";


describe('Card Controller Test', () => {

    let database: Database = null;
    let app: App = null;
    let request = null;

    beforeAll(async () => {
        app = new App();
        await app.run();

        database = app.getDatabase();
        // await database.clearDatabase();
        await seed();
        request = request2(app.express);
    });

    it('should not authenticate', async (done) => {
        const res = await request.get('/card/check/123456789');
        expect(res.status).toBe(401);
        expect(res.body.message).toBe('Unauthorized');
        done();
    });

    it('should fail param validation', async (done) => {
        const res = await request
            .get('/card/check/test')
            .set('x-api-key', 'random---test');
        expect(res.status).toBe(400);
        expect(res.body.message).toBe('Invalid value - parameter: cardId');
        done();
    });

    afterAll(() => {
        database.stop();
        app.stop();
    })
})