import Database from "../src/core/database";
import {seed} from "../src/fixture/user.fixture";
import App from "../src/core/app";
import * as request2 from "supertest";


describe('Card Service Test', () => {

    let database: Database = null;
    let app: App = null;
    let request = null;

    beforeAll(async () => {
        app = new App();
        await app.run();

        database = app.getDatabase();
        // await database.clearDatabase();
        await seed();
        request = request2(app.express);
    });

    it('should authenticate and get results', async (done) => {
        const res = await request
            .get('/card/check/123456789')
            .set('x-api-key', 'random---test');
        expect(res.status).toBe(200);
        expect(res.body).toHaveProperty('state');
        expect(res.body).toHaveProperty('validity_end');
        done();
    });

    it('should authenticate and test api mocked data', async (done) => {
        const res = await request
            .get('/card/check/123456789')
            .set('x-api-key', 'random---test');
        expect(res.status).toBe(200);
        expect(res.body.state).toBe('Aktivní v držení klienta');
        expect(res.body.validity_end).toBe('12.8.2020');
        done();
    });

    afterAll(() => {
        database.stop();
        app.stop();
    })
})